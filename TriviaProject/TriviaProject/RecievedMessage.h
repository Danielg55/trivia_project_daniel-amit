#pragma once
#include "User.h"
#include "DataBase.h"
#include <map>
#include <queue>
#include <thread>
#include <iostream>
#include <mutex>
#include <condition_variable>
#include "Helper.h"
using namespace std;
class Room;

class RecievedMessage
{
public:
	RecievedMessage(SOCKET sock, int msgCode);
	RecievedMessage(SOCKET sock, int msgCode, vector<string> values);
	~RecievedMessage();

	//Getters
	SOCKET getSock();
	User* getUser();
	int getMessageCode();
	vector<string>& getValues();

	//Setters
	void setUser(User* user);

private:
	SOCKET _sock;
	User* _user;
	int _messageCode;
	vector<string> _values;
};
