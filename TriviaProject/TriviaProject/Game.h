/*#pragma once

#include "User.h"
#include "Question.h"
#include "DataBase.h"
#include <map>
#include "Helper.h"

class Room;
class User;
*/

#pragma once
#include "DataBase.h"
//#include "User.h"
#include <vector>
#include <map>
#include <iostream>
#include "User.h"
using namespace std;
class Room;
class User;
class Game
{
public:
	Game(const vector<User*>&, int, DataBase&);
	~Game();
	void sendFirstQuestion();
	void handleFinishGame();
	bool handleNextTurn();
	bool handleAnswerFromUser(User*, int, int);
	bool leaveGame(User*);
	int getID();

private:
	vector<Question*> _questions;
	vector<User*> _players;
	int _questions_no;
	int _currQuestionIndex;
	DataBase& _db;
	map<string, int> _results;
	int _currentTurnAnswers;
	int _id;

	bool insertGameToDB();
	void initQuestionFromDB();
	void sendQuestionToAllUsers();

};