#pragma once

#include <iostream>

using namespace std;

class Question
{
	public:
		Question(int id, string question, string correctAnswer, string answer2, string answer3, string answer4);
		~Question();
		string getQusetion();
		string* getAnswers();
		int getCorrectAnswerIndex();
		int getId();

	private:
		string _question, _answers[4];
		int _correctAnswerIndex, _id;
};