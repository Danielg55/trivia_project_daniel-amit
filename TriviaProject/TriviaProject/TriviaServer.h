#pragma once
#include "User.h"
#include "Room.h"
#include "DataBase.h"
#include "RecievedMessage.h"
#include <map>
#include <queue>
#include <thread>
#include <iostream>
#include <mutex>
#include <condition_variable>
#include "Helper.h"
#include <sstream>

#define PORT 8888 ////////////////////////////

using namespace std;

class TriviaServer
{
public:
	TriviaServer();
	~TriviaServer();

	void server();


private:
	SOCKET _socket;
	map<SOCKET, User*> _connectedUsers;
	DataBase *_db;
	map<int, Room*> _roomList;
	mutex _mtxRecivedMessages;
	queue<RecievedMessage*> _queRvcMessages;

	int _roomIdSequence;

	void bindAndListen();
	void accept();
	void clientHandler(SOCKET client_socket);
	void safeDeleteUser(RecievedMessage* msg);

	RecievedMessage* buildRecieveMessage(SOCKET client_socket, int msgCode);
	void addRecievedMessage(RecievedMessage* msg);
	User* getUserByName(string username);
	User* getUserBySocket(SOCKET client_socket);

	void handleSignout(RecievedMessage* msg);
	User* handleSignin(RecievedMessage* msg);
	bool handleCloseRoom(RecievedMessage * msg);
	void handleLeaveGame(RecievedMessage * msg);
	bool handleLeaveRoom(RecievedMessage* msg);
	bool handleSignup(RecievedMessage* msg);
	void handleStartGame(RecievedMessage* msg);
	void handlePlayerAnswer(RecievedMessage* msg);
	bool handleJoinRoom(RecievedMessage* msg);
	Room* getRoomById(int id);
	bool handleCreateRoom(RecievedMessage* msg);
	void handleGetUsersInRoom(RecievedMessage* msg);
	void handleGetRooms(RecievedMessage* msg);
	void handleGetBestScores(RecievedMessage* msg);
	void handleGetPersonalStatus(RecievedMessage* msg);

	int getNewid();
	Room* getRoomByAdmin(User* admin);
};

