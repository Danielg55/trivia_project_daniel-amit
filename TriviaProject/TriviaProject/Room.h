#pragma once
#include <vector>
#include <iostream>
#include "User.h"
using namespace std;
class Game;
class User;

class Room
{
public:
	Room(int id, User* admin, string name, int maxUsers, int questionsNo, int questionTime);
	~Room();
	bool joinRoom(User*);
	void leaveRoom(User*);
	int closeRoom(User*);
	vector<User*> getUsers();
	string getUsersListMessage();
	int getQuestionNo();
	int getId();
	string getName();
	string getUsersAsString(vector<User*>, User*);
	void sendMessage(string message);
	void sendMessage(User*, string);
	void startGame();
	User* getAdmin();

private:
	vector<User*> _users;
	User* _admin;
	int _maxUsers, _questionTime, _questionsNo, _id;
	string _name;
	DataBase _db;
};