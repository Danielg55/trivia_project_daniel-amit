#pragma once
#include <vector>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <sstream>
#include <string>
#include "Question.h"
#include "Validator.h"
#include "sqlite3.h"

using namespace std;

#define DB_FILE "database.db"

class DataBase
{
public:
	DataBase();
	~DataBase();

	bool doesUserExist(string username);
	bool addNewUser(string username, string password, string email);
	bool doesUserAndPassMatch(string username, string password);
	vector<Question*> initQuestions(int);
	vector<string> getBestScores();
	vector<string> getPersonalStatus(string username);
	int insertNewGame();
	bool updateGameStatus(int gameID);
	bool addAnswerToPlayer(int gameID, string username, int questionId, string answer, bool isCorrect, int answerTime);
	int addNewUserRetCode(string userName, string pass, string email);


private:
	sqlite3* _db;

	void createTables();
};