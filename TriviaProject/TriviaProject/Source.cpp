#include "TriviaServer.h"
#include "WSAInitializer.h"
#include <iostream>
#include <fstream>

// In a lot of places in the code we pass to function constant reference (const Bla&)
// to an object and not the object itself, 

void main()
{
	try
	{
		// NOTICE at the end of this block the WSA will be closed 
		WSAInitializer wsa_init;
		TriviaServer md_server;
		md_server.server();
	}
	catch (const std::exception& e)
	{
		std::cout << "Exception was catch in function: " << e.what() << std::endl;
	}
	catch (...)
	{
		std::cout << "Unknown exception in main !" << std::endl;
	}
}