#include "Question.h"
#include <algorithm> 
#include <vector>

Question::Question(int id, string question, string correctAnswer, string answer2, string answer3, string answer4) : _id(id), _question(question)
{
	for (int i = 0; i < 4; i++)
	{
		vector<int> indexes = { 0, 1, 2, 3 };
		random_shuffle(indexes.begin(), indexes.end());
		_correctAnswerIndex = indexes[0];
		_answers[indexes[0]] = correctAnswer;
		_answers[indexes[1]] = answer2;
		_answers[indexes[2]] = answer3;
		_answers[indexes[3]] = answer4;
	}
}

Question::~Question(){}

string Question::getQusetion(){ return _question; }
string* Question::getAnswers(){ return _answers; }
int Question::getCorrectAnswerIndex(){ return _correctAnswerIndex; }
int Question::getId(){ return _id; }