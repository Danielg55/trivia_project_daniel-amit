#pragma once
#include <string>
#include "Helper.h"
#include "Game.h"
#include "Room.h"
using namespace std;

using namespace std;

class User
{
public:
	User(string username, SOCKET sock);
	~User();
	void send(string message);
	string getUsername();
	SOCKET getSocket();
	Room* getRoom();
	Game* getGame();
	void setGame(Game*);
	bool createRoom(int, string, int, int, int);
	bool joinRoom(Room*);
	void leaveRoom();
	int closeRoom();
	bool leaveGame();
	void clearGame();
	void clearRoom();

private:
	string _username;
	Room* _currRoom;
	Game* _currGame;
	SOCKET _sock;
};