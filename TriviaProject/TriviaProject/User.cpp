#include "User.h"

User::User(string username, SOCKET sock) : _sock(sock), _username(username), _currGame(nullptr), _currRoom(nullptr){}

User::~User() {}

void User::send(string message)
{ 
	try
	{
		Helper::sendData(_sock, message);
	}
	catch (exception e)
	{
		throw e;
	}
}

string User::getUsername(){ return _username; }
SOCKET User::getSocket(){ return _sock; }
Room* User::getRoom(){ return _currRoom; }
Game* User::getGame(){ return _currGame; }
void User::setGame(Game* game){ _currGame = game; _currRoom = nullptr; }

bool User::createRoom(int roomId, string roomName, int maxUsers, int questionsNo, int questionTime)
{
	string msg = "114";
	bool retVal = false;
	if (_currRoom == nullptr)
	{
		_currRoom = new Room(roomId, this, roomName, maxUsers, questionsNo, questionTime);
		msg += "0";
		retVal = true;
	}
	else
	{
		msg += "1";
	}

	try
	{
		send(msg);
	}
	catch (exception e)
	{
		cout << e.what() << endl;
	}

	return retVal;
}

bool User::joinRoom(Room* newRoom)
{
	bool retVal = false;
	if (_currRoom != nullptr)
	{
		if (newRoom != nullptr)
		{
			_currRoom = newRoom;
			retVal = _currRoom->joinRoom(this);
		}
	}

	return retVal;
}

void User::leaveRoom()
{
	if (_currRoom != nullptr)
	{
		_currRoom->leaveRoom(this);
		_currRoom = nullptr;
	}
}

int User::closeRoom()
{
	int temp = 0;
	
	if (_currRoom != nullptr)
	{
		temp = _currRoom->closeRoom(this);
		if (temp != -1)
		{
			temp = _currRoom->getId();
			delete _currRoom;
			_currRoom = nullptr;
		}
	}
	else
		temp = -1;
	return temp;
}

bool User::leaveGame() ////////////not sure
{
	bool retVal = true;
	if (_currRoom != nullptr)
	{
		_currRoom->leaveRoom(this);
		retVal = false;
	}
	return retVal;
}

void User::clearGame(){ _currGame = nullptr; }

void User::clearRoom() { _currRoom = nullptr; }
