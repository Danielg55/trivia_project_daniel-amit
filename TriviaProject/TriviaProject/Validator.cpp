#include "Validator.h"
#include <ctype.h>

bool Validator::isPasswordValid(string password)
{
	/*if (password.length() <= 4 || password.find(" ") == string::npos)
		return false;
	for (int i = 0; i < password.length(); i++)
	{
		if (isdigit(password[i]))
			break;
		if (i == password.length() - 1)
			return false;
	}
	for (int i = 0; i < password.length(); i++)
	{
		if (isupper(password[i]))
			break;
		if (i == password.length() - 1)
			return false;
	}
	for (int i = 0; i < password.length(); i++)
	{
		if (islower(password[i]))
			break;
		if (i == password.length() - 1)
			return false;
	}

	return true;*/
	bool hasDigit = false;
	bool hasBigLetter = false;
	bool hasSmallLetter = false;
	for (int i = 0; i < password.length(); i++)
	{
		if (isdigit(password[i])) hasDigit = true;
		else if (isupper(password[i])) hasBigLetter = true;
		else if (islower(password[i])) hasSmallLetter = true;
	}
	return (password.size() >= 4 && std::string::npos == password.find(' ') && hasBigLetter && hasSmallLetter && hasDigit);
}

bool Validator::isUsernameValid(string username)
{
	if (username.empty() || !isalpha(username[0]))
		return false;

	return true;
}