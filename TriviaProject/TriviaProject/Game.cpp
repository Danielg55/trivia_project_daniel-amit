#include "Game.h"

Game::Game(const vector<User*>& users, int, DataBase& db) : _db(db)
{
	if (insertGameToDB())
		throw exception("Error! Can't insert new game.");
	initQuestionFromDB();
	_players = users;
	for (int i = 0; i < _players.size(); i++)
	{
		_players[i]->setGame(this);
		_results[_players[i]->getUsername()] = 0;
	}
}

Game::~Game()
{
	for (int i = 0; i < _questions.size(); i++)
	{
		delete _questions[i];
	}
}

void Game::sendFirstQuestion()
{
	sendQuestionToAllUsers();
}

void Game::handleFinishGame()
{
	Helper *h = new Helper();
	string msg = "121";
	for (map<string, int>::iterator it = _results.begin(); it != _results.end(); it++)
	{
		msg += h->getStringWithSize(it->first, 2);
		msg += h->getStringWithSize(to_string(it->second), 2);
	}
	_db.updateGameStatus(getID());
	for (int i = 0; i < _players.size(); i++)
	{
		try
		{
			_players[i]->send(msg);
		}
		catch (exception e)
		{
			cout << e.what() << endl;
		}
	}
}

bool Game::handleNextTurn()
{
	if (_players.size() == 0)
	{
		handleFinishGame();
		return false;
	}
	//////////continue
}

bool Game::handleAnswerFromUser(User* user, int answerNo, int time)
{
	_currentTurnAnswers++;

	return true;
}

bool Game::leaveGame(User* currUser)
{
	string tempUsername = currUser->getUsername();
	for (int i = 0; i < _players.size(); i++)
	{
		if (_players[i]->getUsername() == tempUsername)
		{
			_players.erase(_players.begin()+i);
			break;
		}
	}
	handleNextTurn();
	if (_players.size() == 0)
	{
		handleFinishGame();
		return false;
	}
	return true;
}

int Game::getID()
{
	return _id;
}

bool Game::insertGameToDB()
{
	return ((_id = _db.insertNewGame()) < 0);
}

void Game::initQuestionFromDB()
{
	_questions = _db.initQuestions(_questions_no);
}

void Game::sendQuestionToAllUsers()
{
	Helper *h = new Helper();
	Question* currQ = _questions[_currQuestionIndex];
	string* answers = currQ->getAnswers();
	string msg = "118";
	if (currQ != nullptr)
	{
		h->getStringWithSize(currQ->getQusetion(), 3);
		for (int i = 0; i < 4; i++)
		{
			msg += h->getStringWithSize(answers[i], 3);
		}
	}
	else
	{
		msg += "0";
	}
	_currentTurnAnswers = 0;
	for (int i = 0; i < _players.size(); i++)
	{
		try
		{
			_players[i]->send(msg);
		}
		catch (exception e)
		{
			cout << e.what() << endl;
		}
	}
}

