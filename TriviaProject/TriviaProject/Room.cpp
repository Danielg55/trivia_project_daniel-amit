#include "Room.h"

Room::Room(int id, User* admin, string name, int maxUsers, int questionNo, int questionTime) :_id(id), _admin(admin), _name(name), _maxUsers(maxUsers), _questionsNo(questionNo), _questionTime(questionTime)
{
	_users.push_back(admin);
}

Room::~Room() {}

bool Room::joinRoom(User* user)
{
	Helper *h = new Helper();
	string msg = "110";
	bool retVal = false;
	if (_users.size() < _maxUsers)
	{
		_users.push_back(user);
		msg += "0" + h->getSizeFormat(_questionsNo, 2) + h->getSizeFormat(_questionTime, 2);
		retVal = true;
	}
	else
	{
		msg += "1";
	}

	try
	{
		user->send(msg);
	}
	catch(exception c)
	{
		cout << c.what() << endl;
	}
	return retVal;
}

void Room::leaveRoom(User* user)
{
	Helper *h = new Helper();
	string msg = "108";
	int tempSize = _users.size(), i = 0;
	for (; i < tempSize; i++)
	{
		if (_users[i] == user)
		{
			_users.erase(_users.begin()+i);//std::remove(_users.begin(), _users.end(), i), _users.end());
			try
			{
				user->send("1140");
			}
			catch (exception e)
			{
				cout << e.what() << endl;
			}
			break;
		}
	}
	if (i == tempSize)
	{
		try
		{
			user->send("1141");
		}
		catch (exception e)
		{
			cout << e.what() << endl;
		}
	}

	msg += h->getStringWithSize(to_string(_users.size()), 1);
	tempSize = _users.size();
	for (int i = 0; i < tempSize; i++)
	{
		msg += h->getStringWithSize(_users[i]->getUsername(), 2);
	}

	for (int i = 0; i < tempSize; i++)
	{
		try
		{
			_users[i]->send(msg);
		}
		catch (exception e)
		{
			cout << e.what() << endl;
		}
	}
}

int Room::closeRoom(User* user)
{
	string msg = "116";
	if (user == _admin)
	{
		for (int i = 0; i < _users.size(); i++)
		{
			try
			{
				_users[i]->send(msg);
				_users[i]->clearRoom();
			}
			catch (exception e)
			{
				cout << e.what() << endl;
			}
		}

		return _id;
	}
	else
		return -1;
}

vector<User*> Room::getUsers(){ return _users; }

string Room::getUsersListMessage()
{
	string connectedUsers = "108";
	connectedUsers.append(to_string(_users.size()));
	for (auto user : _users)
	{
		connectedUsers += Helper::getPaddedNumber(user->getUsername().size(), 2);
		connectedUsers += user->getUsername();
	}

	return connectedUsers;
}

int Room::getQuestionNo(){ return _questionsNo; }
int Room::getId(){ return _id; }
string Room::getName(){ return _name; }

string Room::getUsersAsString(vector<User*> usersList, User* excludeUser)
{
	string retVal = "";
	for (int i = 0; i < usersList.size(); i++)
	{
		if (usersList[i] != excludeUser)
			retVal += usersList[i]->getUsername();
	}
	return retVal;
}

void Room::sendMessage(string message)
{
	sendMessage(nullptr, message);
}

void Room::sendMessage(User* excludeUser, string message)
{
	for (int i = 0; i < _users.size(); i++)
	{
		if (_users[i] != excludeUser)
		{
			try
			{
				_users[i]->send(message);
			}
			catch (exception e)
			{
				cout << e.what() << endl;
			}
		}
	}
}

void Room::startGame()
{
	Game* game = new Game(_users, _questionsNo, _db);//db here
	for (auto currUser : _users)
	{
		currUser->setGame(game);
	}
}

User* Room::getAdmin() { return _admin; }