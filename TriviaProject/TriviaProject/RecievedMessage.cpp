#include "RecievedMessage.h"


RecievedMessage::RecievedMessage(SOCKET sock, int msgCode)
{
	_sock = sock;
	_messageCode = msgCode;
	_user = nullptr;
}

RecievedMessage::RecievedMessage(SOCKET sock, int msgCode, vector<string> values)
{
	_sock = sock;
	_messageCode = msgCode;
	_values = values;
}

RecievedMessage::~RecievedMessage()
{
}

SOCKET RecievedMessage::getSock() { return _sock; }

User* RecievedMessage::getUser() { return _user; }

int RecievedMessage::getMessageCode() { return _messageCode; }

vector<string>& RecievedMessage::getValues() { return _values; }

void RecievedMessage::setUser(User* user) { _user = user; }
