#include "TriviaServer.h"

TriviaServer::TriviaServer()
{
	_db = new DataBase();	
	_roomIdSequence = 0;
	
	_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_socket == INVALID_SOCKET)
		throw std::exception("Error! - can not open new socket");
}


TriviaServer::~TriviaServer()
{
	for (map<int, Room*>::iterator it = _roomList.begin(); it != _roomList.end(); it++)
		delete it->second;
	for (map<SOCKET, User*>::iterator it = _connectedUsers.begin(); it != _connectedUsers.end(); it++)
		delete it->second;
	try
	{
		closesocket(_socket);
	}
	catch (...) {}
}

void TriviaServer::server()
{
	bindAndListen();
	while (true)
		accept();
}

void TriviaServer::bindAndListen()
{
	/*struct sockaddr info;
	info.sa_family = AF_INET;
	if (::bind(_socket, &info, sizeof(info)) == SOCKET_ERROR)
		throw exception("Error! - bind");

	if (::listen(_socket, SOMAXCONN) == SOCKET_ERROR)
		throw exception("Error! - listen");*/
	struct sockaddr_in sa = { 0 };
	sa.sin_port = htons(PORT);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = 0;
	// again stepping out to the global namespace
	if (::bind(_socket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	TRACE("binded");

	if (::listen(_socket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	TRACE("listening...");
}

void TriviaServer::accept()
{
	SOCKET client_socket = ::accept(_socket, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
		throw exception("Error! - accept");

	// create new thread for client	and detach from it
	thread tr(&TriviaServer::clientHandler, this, client_socket);
	tr.detach();
}

void TriviaServer::clientHandler(SOCKET client_socket)
{
	/*int msgType = 0;
	string msg;
	do
	{
		msgType = Helper::getMessageTypeCode(client_sock);
		addRecievedMessage(buildRecieveMessage(client_sock, msgType));

	} while (msgType != 0 && msgType != 121); /////////////////121 - not sure

	//////////////////////////////
	*/

	while (true)
	{
		try
		{
			int code = Helper::getMessageTypeCode(client_socket);
			TRACE("(%d)", code);
			RecievedMessage* msg = buildRecieveMessage(client_socket, code);
			msg->setUser(getUserBySocket(client_socket));

			if (code == 200) // Ask to sign in
			{
				int username_length = Helper::getIntPartFromSocket(client_socket, 2);
				string username = Helper::getStringPartFromSocket(client_socket, username_length);
				int password_length = Helper::getIntPartFromSocket(client_socket, 2);
				string password = Helper::getStringPartFromSocket(client_socket, password_length);

				if (getUserByName(username) != NULL)
				{
					Helper::sendData(client_socket, "1022"); // user already connected
				}
				else
				{
					if (_db->doesUserExist(username))
					{
						User* user = new User(username, client_socket);
						_connectedUsers.insert(pair<SOCKET, User*>(client_socket, user));
						Helper::sendData(client_socket, "1020"); // success
					}
					else
					{
						Helper::sendData(client_socket, "1021"); // wrong details
					}
				}

			}
			else if (code == 201) // Ask to sign out
			{
				handleSignout(msg);
			}
			else if (code == 203) // Ask to sign up
			{
				int username_length = Helper::getIntPartFromSocket(client_socket, 2);
				string username = Helper::getStringPartFromSocket(client_socket, username_length);
				int password_length = Helper::getIntPartFromSocket(client_socket, 2);
				string password = Helper::getStringPartFromSocket(client_socket, password_length);
				int email_length = Helper::getIntPartFromSocket(client_socket, 2);
				string email = Helper::getStringPartFromSocket(client_socket, email_length);

				string ret_code = to_string(_db->addNewUserRetCode(username, password, email));
				Helper::sendData(client_socket, ret_code); // success
			}
			else if (code == 205) // ask for room list
			{
				map<int, Room*>::iterator it;
				int room_count = 0;
				string message = "";
				for (it = _roomList.begin(); it != _roomList.end(); it++)
				{
					room_count += 1;
				}

				message += "106" + Helper::getPaddedNumber(room_count, 4);
				for (it = _roomList.begin(); it != _roomList.end(); it++)
				{
					message += Helper::getPaddedNumber(it->first, 4);
					message += Helper::getPaddedNumber(it->second->getName().size(), 2);
					message += it->second->getName();
				}
				Helper::sendData(client_socket, message);

			}
			else if (code == 207) // ask for users in room
			{
				try
				{
					int room_id = Helper::getIntPartFromSocket(client_socket, 4);
					Helper::sendData(client_socket, _roomList.find(room_id)->second->getUsersListMessage());
				}
				catch (...)
				{
					Helper::sendData(client_socket, "1080");
				}
			}
			else if (code == 209)//join
			{
				int room_id = Helper::getIntPartFromSocket(client_socket, 4);
				if (getRoomById(room_id) != NULL)
				{
					getRoomById(room_id)->joinRoom(getUserBySocket(client_socket));
				}
				else
				{
					Helper::sendData(client_socket, "1102");
				}
			}
			else if (code == 211)//leave
			{
				getUserBySocket(client_socket)->leaveRoom();//116? 119?
			}
			else if (code == 213)
			{
				try
				{
					int room_sizename = Helper::getIntPartFromSocket(client_socket, 2);
					string room_name = Helper::getStringPartFromSocket(client_socket, room_sizename);
					int room_nplayer = Helper::getIntPartFromSocket(client_socket, 1);
					int room_nquestion = Helper::getIntPartFromSocket(client_socket, 2);
					int room_time = Helper::getIntPartFromSocket(client_socket, 2);
					int room_id = getNewid();
					Room* room = new Room(room_id, getUserBySocket(client_socket), room_name, room_nplayer, room_nquestion, room_time);
					_roomList.insert((pair<int, Room*>(room_id, room)));
					Helper::sendData(client_socket, "1140");
				}
				catch (...)
				{
					Helper::sendData(client_socket, "1141");
				}
			}
			else if (code == 215)
			{
				getRoomByAdmin(getUserBySocket(client_socket))->closeRoom(getUserBySocket(client_socket));
			}
			else if (code == 217)
			{
				getRoomByAdmin(getUserBySocket(client_socket))->startGame();
			}
			else if (code == 219)
			{
				int user_q = Helper::getIntPartFromSocket(client_socket, 1);
				int user_time = Helper::getIntPartFromSocket(client_socket, 2);
				getUserBySocket(client_socket)->getGame()->handleAnswerFromUser(getUserBySocket(client_socket), user_q, user_time);
			}
		}
		catch (const std::exception& e)
		{
			// safeDeleteUser(client_socket);
			std::cout << "Exception was caught in clientHandler: " << e.what() << std::endl;
			TRACE("notify all");
			break;
			//_doc_was_edit.notify_all();
		}
	}
}

void TriviaServer::safeDeleteUser(RecievedMessage* msg) //not sure
{
	User* temp = msg->getUser();
	handleSignout(msg);
	closesocket(temp->getSocket());
	delete temp;
}

RecievedMessage* TriviaServer::buildRecieveMessage(SOCKET client_socket, int msgCode)
{ ////////////////////////////////////////////////////////////
	return new RecievedMessage(client_socket, msgCode);
}

void TriviaServer::addRecievedMessage(RecievedMessage* msg)
{
	_mtxRecivedMessages.lock();
	_queRvcMessages.push(msg);
	_mtxRecivedMessages.unlock();
}

Room* TriviaServer::getRoomById(int id)
{
	return _roomList[id];
}

User* TriviaServer::getUserByName(string username)
{
	//cout << to_string(_connectedUsers.size());
	if (_connectedUsers.size() == 1)
	{
		return NULL;
	}

	for (auto it = _connectedUsers.begin(); it != _connectedUsers.end(); ++it)
		if (it->second->getUsername() == username)
			return it->second;

	return NULL;
}

User* TriviaServer::getUserBySocket(SOCKET client_socket)
{
	return _connectedUsers[client_socket];
}

void TriviaServer::handleSignout(RecievedMessage* msg) //201
{
	User* user = msg->getUser();
	if (user != nullptr)
	{
		_connectedUsers.erase(msg->getSock());
		handleCloseRoom(msg);
		handleLeaveRoom(msg);
		handleLeaveGame(msg);
	}
}

User* TriviaServer::handleSignin(RecievedMessage* msg)
{
	vector<string> temp = msg->getValues();

	if (!_db->doesUserAndPassMatch(temp[0], temp[1]))
		return nullptr;

	return new User(temp[0], msg->getSock());
}

bool TriviaServer::handleCloseRoom(RecievedMessage * msg)
{
	Room* room = msg->getUser()->getRoom();
	int id = 0;
	if (room == nullptr)
		return false;
	id = room->getId();
	if (room->closeRoom(msg->getUser()) != -1)
	{
		_roomList.erase(id);
		return true;
	}
	return false;
}

void TriviaServer::handleLeaveGame(RecievedMessage * msg) //222
{
	if (msg->getUser()->leaveGame())
		delete msg->getUser()->getGame();
}

bool TriviaServer::handleLeaveRoom(RecievedMessage* msg) //211
{
	string tempMsg = "1120";
	User* user = msg->getUser();

	if (user == nullptr || user->getRoom() == nullptr)
		return false;
	user->leaveRoom();
	user->send(tempMsg);
	return true;
}

bool TriviaServer::handleSignup(RecievedMessage* msg)
{
	vector<string> values = msg->getValues();
	string tempMsg = "104";
	bool retVal = false;
	if (!Validator::isPasswordValid(values[1]))
		tempMsg += "1";
	if (tempMsg.size() == 3 && !Validator::isUsernameValid(values[0]))
		tempMsg += "3";
	if (tempMsg.size() == 3 && _db->doesUserExist(values[0]))
		tempMsg += "2";
	if (tempMsg.size() == 3 && !_db->addNewUser(values[0], values[1], values[2]))
		tempMsg += "4";
	if (tempMsg.size() == 3)
	{
		tempMsg += "0";
		retVal = true;
	}

	return retVal;
}

void TriviaServer::handleStartGame(RecievedMessage* msg)//217
{
	User* user = msg->getUser();
	string tempMsg = "118";
	try
	{
		////////////////////////////////
	}
	catch (exception exc)
	{
		tempMsg += "0"; //error
	}

	user->send(tempMsg);
}

void TriviaServer::handlePlayerAnswer(RecievedMessage* msg) //219 /////////////////////////continue
{
	Game* game = msg->getUser()->getGame();
	vector<string> values = msg->getValues();

	if (game != nullptr)
	{
		if (!game->handleAnswerFromUser(msg->getUser(), atoi(values[0].c_str()), atoi(values[1].c_str())))
			delete game;
	}
}

bool TriviaServer::handleJoinRoom(RecievedMessage* msg) //209
{
	User* user = msg->getUser();
	Room* room = nullptr;
	vector<string> values = msg->getValues();
	string tempMsg = "110";
	int roomId = 0;
	if (user == nullptr)
		return false;
	room = getRoomById(atoi(msg->getValues()[0].c_str()));
	if (room != nullptr)
	{
		if (room->joinRoom(user))
		{
			tempMsg += "0" + Helper::getPaddedNumber(atoi(values[0].c_str()), 2) + Helper::getPaddedNumber(atoi(values[1].c_str()), 2);
			user->send(tempMsg);
			return true;
		}
		else
		{
			tempMsg += "1";
			user->send(tempMsg);
			return false;
		}
	}

	tempMsg += "2";
	user->send(tempMsg);

	return false;
}

bool TriviaServer::handleCreateRoom(RecievedMessage* msg) //213
{
	User* user = msg->getUser();
	vector<string> values = msg->getValues();
	if (user == nullptr)
		return false;
	_roomIdSequence++;
	if (user->createRoom(_roomIdSequence, values[0], atoi(values[1].c_str()), atoi(values[2].c_str()), atoi(values[3].c_str())))
	{
		_roomList[_roomIdSequence] = user->getRoom();
		return true;
	}

	return false;
}

void TriviaServer::handleGetUsersInRoom(RecievedMessage* msg) //207
{
	User* user = msg->getUser();
	Room* room = getRoomById(atoi(msg->getValues()[0].c_str()));
	string tempMsg = "108";

	if (room != nullptr)
	{
		tempMsg = room->getUsersListMessage();
	}
	else
	{
		tempMsg += "0";
	}
	user->send(tempMsg);
}

void TriviaServer::handleGetRooms(RecievedMessage* msg) //205
{
	string tempMsg = "106" + Helper::getPaddedNumber(_roomList.size(), 4), name;

	for (map<int, Room*>::iterator it = _roomList.begin(); it != _roomList.end(); it++)
	{
		tempMsg += Helper::getPaddedNumber(it->first, 4) + Helper::getPaddedNumber(it->second->getName().size(), 2) + it->second->getName();
	}

	msg->getUser()->send(tempMsg);
}

void TriviaServer::handleGetBestScores(RecievedMessage* msg) //223
{
	_db->getBestScores(); ///////////////////////////
}

void TriviaServer::handleGetPersonalStatus(RecievedMessage* msg) //225
{
	_db->getPersonalStatus(msg->getUser()->getUsername());//////////////////////
}

int TriviaServer::getNewid(){ return ++_roomIdSequence; }

Room* TriviaServer::getRoomByAdmin(User* admin)
{
	for (auto currRoom : _roomList)
	{
		if (currRoom.second->getAdmin() == admin) return currRoom.second;
	}
	return NULL;
}
