#include "DataBase.h"
#include <unordered_map>
#include "AVLTree.h"
#include "BSNode.h"

unordered_map<string, vector<string>> results;
int callback(void* notUsed, int argc, char** argv, char** azCol);

DataBase::DataBase()
{
	string temp;
	int rc = sqlite3_open(DB_FILE, &_db);

	if (rc)
	{
		temp = string("Can't open database: \n") + string(sqlite3_errmsg(_db));
		sqlite3_close(_db);
		throw exception(temp.c_str());
	}

	createTables();

}

void DataBase::createTables()
{
	string temp = "select name from sqlite_master where type='table' and (name='t_users' or name='t_games' or name='t_questions' or name='t_players_answers');";
	char* zErrMsg = NULL;
	int rc = sqlite3_exec(_db, temp.c_str(), callback, NULL, &zErrMsg);

	if (rc)
	{
		temp = "SQL error: " + string(zErrMsg);
		sqlite3_free(zErrMsg);
		throw exception(temp.c_str());
	}

	if (results.size() != 4)
	{
		temp = "PRAGMA foreign_keys = 1; ";
		rc = sqlite3_exec(_db, temp.c_str(), callback, NULL, &zErrMsg);
		if (rc)
			sqlite3_free(zErrMsg);

		temp = "create table t_users(username text not null, password text not null, email text nut null, primary key(username));";
		rc = sqlite3_exec(_db, temp.c_str(), callback, NULL, &zErrMsg);
		if (rc)
			sqlite3_free(zErrMsg);

		temp = "create table t_games(game_id integer primary key autoincrement not null, status integer not null, start_time datetime not null, end_time datetime);";
		rc = sqlite3_exec(_db, temp.c_str(), callback, NULL, &zErrMsg);
		if (rc)
			sqlite3_free(zErrMsg);

		temp = "create table t_questions(question_id integer primary key autoincrement not null, question text not null, correct_ans text not null, ans2 text not null, ans3 text not null, ans4 text not null);";
		rc = sqlite3_exec(_db, temp.c_str(), callback, NULL, &zErrMsg);
		if (rc)
			sqlite3_free(zErrMsg);

		temp = "create table t_players_answers(game_id integer not null, username text not null, question_id integer not null, player_answer text not null, is_correct integer not null, answer_time datetime not null, primary key(question_id, username, game_id), foreign key(username) references t_users(username), foreign key(game_id) references t_games(game_id), foreign key(question_id) references t_questions(question_id));";
		rc = sqlite3_exec(_db, temp.c_str(), callback, NULL, &zErrMsg);
		if (rc)
			sqlite3_free(zErrMsg);
	}
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

DataBase::~DataBase()
{
	sqlite3_close(_db);
}

bool DataBase::doesUserExist(string username)
{
	results.clear();
	char* zErrMsg = NULL;
	string temp = "select * from t_users where username='" + username + "';";
	int rc = sqlite3_exec(_db, temp.c_str(), callback, NULL, &zErrMsg);
	if (rc)
	{
		temp = "SQL error: " + string(zErrMsg);
		sqlite3_free(zErrMsg);
		throw exception(temp.c_str());
	}
	if (results.empty())
	{
		return false;
	}
	return true;
}

bool DataBase::addNewUser(string username, string password, string email)
{
	char* zErrMsg = NULL;
	string temp = "insert into t_users (username, password, email) values('" + username + "', '"+password+"', '"+email+"');";
	int rc = sqlite3_exec(_db, temp.c_str(), callback, NULL, &zErrMsg);
	if (rc)
	{
		temp = "SQL error: " + string(zErrMsg);
		sqlite3_free(zErrMsg);
		throw exception(temp.c_str());
		return false;
	}
	return true;
}

bool DataBase::doesUserAndPassMatch(string username, string password)
{
	results.clear();
	char* zErrMsg = NULL;
	string temp = "select * from t_users where username='" + username + "' and password='"+password+"';";
	int rc = sqlite3_exec(_db, temp.c_str(), callback, NULL, &zErrMsg);
	if (rc)
	{
		temp = "SQL error: " + string(zErrMsg);
		sqlite3_free(zErrMsg);
		throw exception(temp.c_str());
	}
	if (results.empty())
	{
		return false;
	}
	return true;
}

vector<Question*> DataBase::initQuestions(int questionsNo)
{
	results.clear();
	BSNode<int>* avl = new AVLTree<int>(-1);
	vector<Question*> retVal;
	char* zErrMsg = nullptr;
	int selectedQuestion = 0, questionsSize = 0, counter = 0;
	string temp = "select * from t_questions;";
	int rc = sqlite3_exec(_db, temp.c_str(), callback, NULL, &zErrMsg);
	if (rc)
	{
		temp = "SQL error: " + string(zErrMsg);
		sqlite3_free(zErrMsg);
		throw exception(temp.c_str());
	}
	questionsSize = results.at("question_id").size();

	do
	{
		selectedQuestion = rand() % questionsSize;
		if (!avl->search(selectedQuestion))
		{
			avl = avl->insert(selectedQuestion);
			retVal.push_back(new Question(stoi(results.at("question_id")[counter]), results.at("question")[counter], results.at("correct_ans")[counter], results.at("ans2")[counter], results.at("ans3")[counter], results.at("ans4")[counter]));
			counter++;
		}
	} while (counter < questionsNo);
	return retVal;
}

vector<string> DataBase::getBestScores()
{
	vector<string> q;
	return q;
}

vector<string> DataBase::getPersonalStatus(string username)
{
	results.clear();
	vector<string> retVal;

	char* zErrMsg = NULL;
	string temp1 = "select * from t_players_answers where username = '" + username + "' and is_correct = 'true';";
	int rc1 = sqlite3_exec(_db, temp1.c_str(), callback, NULL, &zErrMsg);

	double ansTimeAvg = 0;
	for (int i = 0; i < results["answer_time"].size(); i++)
	{
		ansTimeAvg += stoi(results["answer_time"][i]);
	}

	ansTimeAvg /= results["answer_time"].size();
	retVal.push_back(username); // username
	retVal.push_back(to_string(results["answer_time"].size())); //num of correct answers
	retVal.push_back(to_string(ansTimeAvg)); // avg time to answer correctly 


	return retVal;
}

int DataBase::insertNewGame()
{
	results.clear();
	char* zErrMsg = NULL;
	string temp = "insert into t_games(status, start_time, end_time) values(0, datetime(), null);";
	int rc = sqlite3_exec(_db, temp.c_str(), callback, NULL, &zErrMsg);

	temp = "select game_id from t_games where status = 0;";
	rc = sqlite3_exec(_db, temp.c_str(), callback, NULL, &zErrMsg);

	unordered_map<string, vector<string>>::iterator it;

	for (auto it : results)
	{
		return atoi((it.second)[0].c_str());
	}
	return 0;
}

bool DataBase::updateGameStatus(int gameID)
{
	results.clear();
	char* zErrMsg = NULL;
	string temp = "update t_games set status = 1, end_time = datetime() where game_id = " + to_string(gameID) + ";";
	int rc = sqlite3_exec(_db, temp.c_str(), callback, NULL, &zErrMsg);

	if (rc) return false;

	return true;
}

bool DataBase::addAnswerToPlayer(int gameID, string username, int questionId, string answer, bool isCorrect, int answerTime)
{
	char* zErrMsg = NULL;

	string temp = "insert into t_players_answers(game_id, username, question_id, player_answer, is_correct, answer_time) values(" + to_string(gameID) + ", '" + username + "' , " + to_string(questionId) + ", '" + answer + "', " + '\'' + (isCorrect ? "true" : "false") + '\'' + ", " + '\'' + to_string(answerTime) + '\'' + ");";
	int rc = sqlite3_exec(_db, temp.c_str(), callback, NULL, &zErrMsg);

	if (rc)
	{
		temp = "SQL error: " + string(zErrMsg);
		sqlite3_free(zErrMsg);
		throw exception(temp.c_str());
		return false;
	}

	return true;
}

int DataBase::addNewUserRetCode(string userName, string pass, string email)
{
	if (!Validator::isPasswordValid(pass))   return 1041;//pass invalid
	if (!Validator::isUsernameValid(userName))   return 1043;// user name invalid
	if (doesUserExist(userName))   return 1042;
	if (addNewUser(userName, pass, "email@example.com")) return 1040;
	return 1044; //something else happened 
}