#pragma once
#include <iostream>
#include <string>
using namespace std;

class Validator
{
public:
	bool static isPasswordValid(string password);
	bool static isUsernameValid(string username);
};